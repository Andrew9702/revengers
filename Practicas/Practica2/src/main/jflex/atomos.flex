package asintactico;

%%
%class Letras
%public
%unicode
%byaccj

%{
    private Parser yyparser;

    /** Nuevo constructor
    * @param FileReader r
    * @param parser parser - parser
    */
    public Letras(java.io.Reader r, Parser parser){
    	   this(r);
    	   yyparser = parser;
    }
%}

ENTERO        = [1-9][0-9]* | 0+
MAS      = \+ 
MULT =  \* 
MENOS =  \- 
DIV =  \/ 


%%
{ENTERO}    {yyparser.yylval = new ParserVal(yytext()); return Parser.ENTERO;}
{MAS}       {yyparser.yylval = new ParserVal(yytext()); return Parser.MAS;}
{MULT}      {yyparser.yylval = new ParserVal(yytext()); return Parser.MULT;}
{MENOS}     {yyparser.yylval = new ParserVal(yytext()); return Parser.MENOS;}
{DIV}       {yyparser.yylval = new ParserVal(yytext()); return Parser.DIV;}
"\n"        {}
" "         {}
[^]         {System.out.println("ERROR: Token no reconocido.");
             System.exit(1);}