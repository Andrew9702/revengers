// parser.y
%{

import java.io.*;

%}

%token<sval> ENTERO MAS MENOS MULT DIV
%type<sval> start E T F    

%%

/*Se aniade la gramatica 1*/
start:   {System.out.println("[OK] " + $$);}
     | E {System.out.println("[OK] " + $$);}

E: T {$$ = $1;}
 | E MAS T   {$$ = $1 + $2 + $3; dump_stacks(stateptr);}
 | E MENOS T   {$$ = $1 + $2 + $3; dump_stacks(stateptr);}
 ;

T: F {$$ = $1;}
| T MULT F   {$$ = $1 + $2 + $3; dump_stacks(stateptr);}
| T DIV F   {$$ = $1 + $2 + $3; dump_stacks(stateptr);}
;

F: ENTERO {$$ = $1; dump_stacks(stateptr);}
| MENOS ENTERO   {$$ = $1 + $2; dump_stacks(stateptr);}
;


%%

private Letras alexico;

// Regresar átomos
private int yylex() {
  int yyl_return = -1;

  try{
    yyl_return = alexico.yylex();

  }catch (IOException e){
    System.err.println("Error de IO." + e);
  }
  return yyl_return;
}

public void yyerror (String error){
  System.err.println("[ERROR] " +error);
  System.exit(2);
}

public Parser(Reader r){
  alexico = new Letras(r,this);
}

public static void main(String args[]){
  try{
   Parser yyparser = new Parser(new FileReader(args[0]));
   yyparser.yyparse();
  }catch(FileNotFoundException e){
    System.err.println("El Archivo " + args[0] + " no existe.");
  }

}
