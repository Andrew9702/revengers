Como correr el proyecto:

	Hacer un clean build desde netbeans para que se genere la clase Lexico de java

	Correr el programa con la clase Test

	Al correrlo se pedira que se introduzca el nombre del archivo a leer sin la extension
	para lo cual se debera introducir como en el ejemplo: fizzbuzz
	
	Solo se deben introducir los archivos cuya extension sea .p

	Al introducirlo presionar enter, posteriormente este generara una salida con el analisis lexico
	ademas de un archivo en la carpeta out, este nuevo archivo tendra el mismo nombre pero con la 
	extension .plx
