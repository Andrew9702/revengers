package lexico;
import java.lang.Exception;
import java.util.Stack;
import java.util.Arrays;
import java.io.FileWriter;
import java.io.IOException;
import java.io.File;
%%

%public
%class Flexer
%unicode
%standalone
%{

//Creamos una pila donde se almacenaran los espacios encontrados.
static Stack<Integer> pila = new Stack<Integer>();

//Inicializamos en cero el numero de espacios.
static int espacios=0;
static int linea = 1;
static FileWriter fw;

/**
    Metodo que almacena en una pila el numero
    de espacios en blanco antes del inicio de una linea.
*/
public static void espacios(int esp){
    //Inicializamos la pila con 0 espacios en blanco
    // y el numero de espacios en parametro.
    if(pila.empty()){
        pila.push(0);
        pila.push(esp);
        System.out.print("INDENTACION (" + pila.peek() + ")");
        try{
            fw.write("INDENTACION (" + pila.peek() + ")");
        }catch(IOException e){
            System.out.println("NO SE PUEDE ABRIR EL ARCHIVO");
            e.printStackTrace();
        }
    }else{
        //Revisamos si el parametro es mayor al ultimo
        // numero de espacios introducido en la pila, esto 
        // significa el inicio de un bloque
        if(esp > pila.peek()){
            int indentacion = pila.push(esp);
            System.out.print("IDENTACION("+indentacion+")");
            try{
                fw.write("INDENTACION (" + indentacion + ")");
            } catch(IOException e){
                System.out.println("NO SE PUEDE ABRIR EL ARCHIVO");
                e.printStackTrace();
            }
        //Revisamos si es el mismo numero
        // lo cual indicaria que la linea pertenece al mismo bloque.
        }else if(esp == pila.peek()){
            System.out.print("("+esp+")");
            try{
                fw.write("("+esp+")");
            }  catch(IOException e){
                System.out.println("NO SE PUEDE ABRIR EL ARCHIVO");
                e.printStackTrace();
            }
        //En el caso de ser menor entonces sacamos el espacio
        //de la pila y significa una deindentacion.
        //Pero si llegamos al primer elemento de la pila y los espacios
        //aun son mas que cero, significa un error de indentacion.
        }else{
            while (esp < pila.peek()){
                int deindentacion = pila.pop();
                System.out.println("DEINDENTA("+deindentacion +")");
                try{
                    fw.write("DEINDENTA("+deindentacion +")");
                }  catch(IOException e){
                    System.out.println("NO SE PUEDE ABRIR EL ARCHIVO");
                    e.printStackTrace();
                }
                if (pila.peek() == 0 && esp !=0){
                    System.out.print("\n ***Error: Mala indentación*** " + " Linea: " + linea + "\n");
                    try{
                        fw.write("\n ***Error: Mala indentación*** " + " Linea: " + linea + "\n");
                        fw.flush();
                        fw.close();
                    }  catch(IOException e){
                            System.out.println("NO SE PUEDE ABRIR EL ARCHIVO");
                            e.printStackTrace();
                    }
                    System.exit(0);
                }
            }
        }
    }

    espacios = 0;   
}
%}

//Ahora limpiamos la pila cuando no haya mas lineas por leer
%eof{
    while(!pila.empty()){
        int ultimo = pila.pop();
        System.out.print("\nDEINDENTA("+ultimo+")");
        try{
            fw.write("\nDEINDENTA("+ultimo+")");
        }catch(IOException e){
                System.out.println("NO SE PUEDE ABRIR EL ARCHIVO");
                e.printStackTrace();
        }
    }
%eof}

%x otro
BOOLEANO      = True | False
ENTERO        = [1-9][0-9]* | 0+
REAL          = \.[0-9]+ | {ENTERO}\.[0-9]+ | {ENTERO}\.
OPERADOR      = \+ | \* | \- | \/ | \= | \> | \< | \>\= | \<\= | \=\= |
                \% | \!\=
CADENA        = "\""[^\\"\""]*"\""
RESERVADA     = and | or | not | for | while | if | else | elif | print
IDENTIFICADOR = ([:letter:] | _ ) ([:jletter:] | [:digit:] | _ )*
SEPARADOR     = \:
SALTO         = "\n"
ESPACIO       = " "
SIMBOLO       = "(" | ")"

%%
#.*              {System.out.print("COMENTARIO"); fw.write("COMENTARIO");}
{BOOLEANO}       {System.out.print("BOOLEANO (" + yytext() + ")"); fw.write("BOOLEANO (" + yytext() + ")");}
{ENTERO}         {System.out.print("ENTERO (" + yytext() + ")"); fw.write("ENTERO (" + yytext() + ")");}
{REAL}           {System.out.print("REAL (" + yytext() + ")"); fw.write("REAL (" + yytext() + ")");}
{OPERADOR}       {System.out.print("OPERADOR (" + yytext() + ")"); fw.write("OPERADOR (" + yytext() + ")");}
{CADENA}         {if(yytext().contains("\\") || yytext().substring(1, yytext().length()-1).contains("\"")){
                   System.out.print("\n ***Error: Cadena erronea*** "+ yytext() + " Linea: " + linea + "\n");
                       fw.write("\n ***Error: Cadena erronea*** " + yytext() + " Linea: " + linea + "\n");
                        fw.flush();
                        fw.close();
                        System.exit(0);
                       }else{ 
                            System.out.print("CADENA(" + yytext() + ")");  
                            fw.write("CADENA(" + yytext() + ")");
                       }
                    }
{RESERVADA}      {System.out.print("RESERVADA (" + yytext() + ")"); fw.write("RESERVADA (" + yytext() + ")");}
{IDENTIFICADOR}  {System.out.print("IDENTIFICADOR (" + yytext() + ")"); fw.write("IDENTIFICADOR (" + yytext() + ")");}
{SEPARADOR}      {System.out.print("SEPARADOR (" + yytext() + ")"); fw.write("SEPARADOR (" + yytext() + ")");}
{ESPACIO}        {}   
{SIMBOLO}        {}
{SALTO}          {System.out.println("SALTO");  yybegin(otro); linea++; fw.write("SALTO\n");}
.                {System.out.println("\n  ***Error: Lexema no identificado*** " + " Linea: " + linea + "\n"); 
                  fw.write("\n  ***Error: Lexema no identificado*** " + " Linea: " + linea + "\n");
                  fw.flush();
                  fw.close();
                  System.exit(0);}

<otro>{
" " {espacios++;}
"/t" {espacios+=4;}
.   {yypushback(1);
    yybegin(YYINITIAL);
    espacios(espacios);}
}
