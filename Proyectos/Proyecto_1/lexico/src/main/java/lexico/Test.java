package lexico;
import java.io.*;
import java.util.Scanner;

public class Test {

    public static void main (String[] args){
        System.out.println("Introduzca nombre sin extension del archivo de prueba");
        Scanner sc = new Scanner(System.in);
        String archivo = sc.nextLine();
        File escribir = new File("out/"+archivo+".plx");
        try {
            FileWriter fw = new FileWriter(escribir);
            AnalizadorLexico al = new AnalizadorLexico("src/main/resources/" + archivo + ".p", fw);
            al.analiza();
            fw.flush();
            fw.close();
        } catch (IOException ex) {
            System.out.println("Error en el archivo");
        }   
    }
    
}