package lexico;
import java.io.*;

public class AnalizadorLexico {
    Flexer lexer;

    public AnalizadorLexico(String archivo, FileWriter escribir) throws IOException{
        try {
            Reader lector = new FileReader(archivo);
            lexer = new Flexer(lector);
            lexer.fw = escribir;
        }
        catch(FileNotFoundException ex) {
            System.out.println(ex.getMessage() + " No se encontró el archivo;");
        }
    }

    public void analiza(){
        try{
          lexer.yylex();
        }catch(IOException ex){
            System.out.println(ex.getMessage());
        }
    }

}