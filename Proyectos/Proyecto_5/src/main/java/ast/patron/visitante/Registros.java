/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ast.patron.visitante;

import java.util.Arrays;

/**
 *
 * @author andres, derian, diana
 */
public class Registros {
    
    //Los 32 registros para enteros.
    int objetivoEntero;
    
    //Los 32 registros para punto flotante
    int objetivoFlotante;

  // Todos los registros enteros disponibles
  String[] E_registros = {"$t0", "$t1", "$t2", "$t3", "$t4", "$t5", "$t6", "$t7", "$t8" ,"$t9"};
  
  // Todos los registros flotantes disponibles
  String[] F_registros = {"$f4", "$f5", "$f6", "$f7", "$f8", "$f9", "$f10"};
  
  
  public void setObjetivo(int o, int verifica_tipo){
      if(verifica_tipo == 3){
        objetivoFlotante = o % F_registros.length;
      } else {
        objetivoEntero = o % E_registros.length;
      }
  }

  public void setObjetivo(String o, int verifica_tipo){
      String[] tipo_r;
      
      if(verifica_tipo == 3){
          tipo_r = F_registros;
      } else {
          tipo_r = E_registros;
      }
      
      int nvo_objetivo = Arrays.asList(tipo_r).indexOf(o);
      setObjetivo(nvo_objetivo, verifica_tipo);
  }

  public String getObjetivo(int verifica_tipo){
      if(verifica_tipo == 3){
        return F_registros[objetivoFlotante];
      } else {
        return E_registros[objetivoEntero];
      }
  }

  /* Regresa los n registos siguientes "disponibles" */
  public String[] getNsiguientes(int n, int verifica_tipo){
      String[] siguientes = new String[n];
      
      if(verifica_tipo == 3){
          
        for(int i = 0; i < n; i++){
            siguientes[i] = F_registros[(objetivoFlotante + i) % F_registros.length];
        }
        
      } else {
          
        for(int i = 0; i < n; i++){
            siguientes[i] = E_registros[(objetivoEntero + i) % E_registros.length];
        }
      }
      return siguientes;

  }
}