/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ast.patron.visitante;

import ast.patron.compuesto.*;
import java.util.Hashtable;
import java.util.LinkedList;

/**
 * 
 *  Definicion de los tipos:
 * 
 *      1 - boolean
 *      2 - entero
 *      3 - real
 *      4 - cadena
 *     -1 - error
 * 
 * @author andres, cderian y diana
 */
public class VisitorTypes implements Visitor{
    
    int variable_encontrada = 0;
    
    private Hashtable<String, Integer> tiposVariables;
    VerificacionTipos v_tipos;
    
    public VisitorTypes() {
        tiposVariables = new Hashtable<String, Integer>();
        v_tipos = new VerificacionTipos();
    }
    
    public void visit(Hoja n) {}
    
    public void visit(Nodo n) {}
    
    public void visit(NodoBinario n) {}
    
    public void visit(NodoExcepcion n){}

    public void visit(HojaCadena n) {
        System.out.println("[Cadena]");
    }

    public void visit(HojaBoolean n) {
        System.out.println("[Boolean]");
    }

    public void visit(HojaReal n) {
        System.out.println("[Real]");
    }

    public void visit(HojaEntero n) {
        System.out.println("[Entero]");
    }

    public void visit(HojaIdentificador n) {
        if(!tiposVariables.containsKey(n.getNombre())){
            System.err.println("\n[Variable]: " + n.getNombre() + " esta definida en la tabla\n");
            System.exit(0);
        }else{
            variable_encontrada =  tiposVariables.get(n.getNombre());
        }
    }

    public void visit(NodoIf n) {
        
        LinkedList<Nodo> hijos = n.getTotal();
        hijos.get(0).accept(this);
        hijos.get(1).accept(this);
        
        if (n.numeroHijos() == 3) {
            hijos.get(2).accept(this);
        }
    }

    public void visit(NodoElse n) {
        n.getPrimerHijo().accept(this);
        n.getUltimoHijo().accept(this);
    }

    public void visit(NodoWhile n) {
        n.getPrimerHijo().accept(this);
        n.getUltimoHijo().accept(this);
    }

    public void visit(NodoLE n) {
        n.getPrimerHijo().accept(this);
        n.getUltimoHijo().accept(this);
        
        int tipo;
        int hijo_izquierdo = n.getPrimerHijo().getType();
        int hijo_derecho = n.getUltimoHijo().getType();
        
        tipo = this.v_tipos.validaComparacion(hijo_izquierdo, hijo_derecho);
        //Si el valor es -1, entonces existe un error
        
        if (tipo == -1) {
            System.out.println("Error : tipos erróneos");
            System.out.println("Menor ");
            System.exit(0);
        }
    }

    public void visit(NodoGR n) {
        n.getPrimerHijo().accept(this);
        n.getUltimoHijo().accept(this);
        
        int tipo;
        int hijo_izquierdo = n.getPrimerHijo().getType();
        int hijo_derecho = n.getUltimoHijo().getType();
        
        tipo = this.v_tipos.validaComparacion(hijo_izquierdo, hijo_derecho);
        //Si el valor es -1, entonces existe un error
        
        if (tipo == -1) {
            System.out.println("Error : tipos erróneos");
            System.out.println("Mayor");
            System.exit(0);
        }
    }

    public void visit(NodoEQ n) {
        n.getPrimerHijo().accept(this);
        n.getUltimoHijo().accept(this);
        
        int tipo;
        int hijo_izquierdo = n.getPrimerHijo().getType();
        int hijo_derecho = n.getUltimoHijo().getType();
        
        tipo = this.v_tipos.validaComparacion(hijo_izquierdo, hijo_derecho);
        //Si el valor es -1, entonces existe un error
        System.out.println(tipo);
        if (tipo == -1) {
            System.out.println("Error : tipos erróneos");
            System.out.println("Igual igual");
            System.exit(0);
        }
    }

    public void visit(NodoGRQ n) {
        n.getPrimerHijo().accept(this);
        n.getUltimoHijo().accept(this);
        
        int tipo;
        int hijo_izquierdo = n.getPrimerHijo().getType();
        int hijo_derecho = n.getUltimoHijo().getType();
        
        tipo = this.v_tipos.validaComparacion(hijo_izquierdo, hijo_derecho);
        //Si el valor es -1, entonces existe un error
        System.out.println("TIPO:" + tipo);
        if (tipo == -1) {
            System.out.println("Error : tipos erróneos");
            System.out.println("Mayor igual");
            System.exit(0);
        }
    }

    public void visit(NodoLEQ n) {
        n.getPrimerHijo().accept(this);
        n.getUltimoHijo().accept(this);
        
        int tipo;
        int hijo_izquierdo = n.getPrimerHijo().getType();
        int hijo_derecho = n.getUltimoHijo().getType();
        
        tipo = this.v_tipos.validaComparacion(hijo_izquierdo, hijo_derecho);
        //Si el valor es -1, entonces existe un error
        if (tipo == -1) {
            System.out.println("Error : tipos erróneos");
            System.out.println("Menor igual");
            System.exit(0);
        }
    }

    public void visit(NodoMenos n) {
        n.getPrimerHijo().accept(this);
        n.getUltimoHijo().accept(this);
        
        int tipo;
        int hijo_izquierdo = n.getPrimerHijo().getType();
        int hijo_derecho = n.getUltimoHijo().getType();
        
        tipo = this.v_tipos.validaComparacion(hijo_izquierdo, hijo_derecho);
        //Si el valor es -1, entonces existe un error
        if (tipo == -1) {
            System.out.println("Error : restando tipos erroneos");
            System.exit(0);
        }
    }

    public void visit(NodoMasIgual n) {
        n.getPrimerHijo().accept(this);
        n.getUltimoHijo().accept(this);
        
        int tipo;
        int hijo_izquierdo = n.getPrimerHijo().getType();
        int hijo_derecho = n.getUltimoHijo().getType();
        
        tipo = this.v_tipos.validaComparacion(hijo_izquierdo, hijo_derecho);
        //Si el valor es -1, entonces existe un error
        if (tipo == -1) {
            System.out.println("Error : tipos erroneos");
            System.out.println("Mas igual");
            System.exit(0);
        }
    }

    public void visit(NodoPor n) {
        n.getPrimerHijo().accept(this);
        n.getUltimoHijo().accept(this);
        
        int tipo;
        int hijo_izquierdo = n.getPrimerHijo().getType();
        int hijo_derecho = n.getUltimoHijo().getType();

        if (n.getPrimerHijo() == null) {
            System.out.println("Invalido");
        }
        
        tipo = this.v_tipos.validaMultiplicacion(hijo_izquierdo, hijo_derecho);
        if (tipo == -1) {
            System.out.println("Error : Multiplicando tipos erróneos");
            System.exit(0);
        }
    }

    public void visit(NodoDivEntera n) {
        n.getPrimerHijo().accept(this);
        n.getUltimoHijo().accept(this);
        
        int tipo;
        int hijo_izquierdo = n.getPrimerHijo().getType();
        int hijo_derecho = n.getUltimoHijo().getType();

        if (n.getPrimerHijo() == null) {
            System.out.println("Invalido");
        }
        
        tipo = this.v_tipos.validaMultiplicacion(hijo_izquierdo, hijo_derecho);
        if (tipo == -1) {
            System.out.println("Error : Division de tipos erróneos");
            System.exit(0);
        }
    }

    public void visit(NodoModulo n) {
        n.getPrimerHijo().accept(this);
        n.getUltimoHijo().accept(this);
        
        int tipo;
        int hijo_izquierdo = n.getPrimerHijo().getType();
        int hijo_derecho = n.getUltimoHijo().getType();

        if (n.getPrimerHijo() == null) {
            System.out.println("Invalido");
        }
        
        tipo = this.v_tipos.validaMultiplicacion(hijo_izquierdo, hijo_derecho);
        if (tipo == -1) {
            System.out.println("Error : Modulo tipos erróneos");
            System.exit(0);
        }
    }

    public void visit(NodoDiv n) {
        n.getPrimerHijo().accept(this);
        n.getUltimoHijo().accept(this);
        
        int tipo;
        int hijo_izquierdo = n.getPrimerHijo().getType();
        int hijo_derecho = n.getUltimoHijo().getType();

        if (n.getPrimerHijo() == null) {
            System.out.println("Invalido");
        }
        
        tipo = this.v_tipos.validaMultiplicacion(hijo_izquierdo, hijo_derecho);
        if (tipo == -1) {
            System.out.println("Error : Division de tipos erróneos");
            System.exit(0);
        }
    }

    public void visit(NodoPotencia n) {
        n.getPrimerHijo().accept(this);
        n.getUltimoHijo().accept(this);
        
        int tipo;
        int hijo_izquierdo = n.getPrimerHijo().getType();
        int hijo_derecho = n.getUltimoHijo().getType();

        if (n.getPrimerHijo() == null) {
            System.out.println("Invalido");
        }
        
        tipo = this.v_tipos.validaMultiplicacion(hijo_izquierdo, hijo_derecho);
        if (tipo == -1) {
            System.out.println("Error : Potencia tipos erróneos");
            System.exit(0);
        }
    }

    public void visit(NodoIgual n) {
        n.getUltimoHijo().accept(this);
        String name = n.getPrimerHijo().getNombre();
        
        if(this.tiposVariables.get(name) == null){
            this.tiposVariables.put(name, n.getUltimoHijo().getType());
        }
        
        int tipo = this.tiposVariables.get(name);
        
        if(tipo != n.getUltimoHijo().getType()){
            System.out.println("Error : De Tipos");
            System.exit(1);
        }
    }

    public void visit(NodoDIFF n) {
        n.getPrimerHijo().accept(this);
        n.getUltimoHijo().accept(this);
        
        int tipo;
        int hijo_izquierdo = n.getPrimerHijo().getType();
        int hijo_derecho = n.getUltimoHijo().getType();

        if (n.getPrimerHijo() == null) {
            System.out.println("Invalido");
        }
        
        tipo = this.v_tipos.validaMultiplicacion(hijo_izquierdo, hijo_derecho);
        if (tipo == -1) {
            System.out.println("Error : diferente tipos erróneos");
            System.exit(0);
        }
    }

    public void visit(NodoAnd n) {
        n.getPrimerHijo().accept(this);
        n.getUltimoHijo().accept(this);
        
        int tipo;
        int hijo_izquierdo = n.getPrimerHijo().getType();
        int hijo_derecho = n.getUltimoHijo().getType();

        if (n.getPrimerHijo() == null) {
            System.out.println("Invalido");
        }
        
        tipo = this.v_tipos.validaMultiplicacion(hijo_izquierdo, hijo_derecho);
        if (tipo == -1) {
            System.out.println("Error : and tipos erróneos");
            System.exit(0);
        }
    }

    public void visit(NodoNot n) {
        n.getPrimerHijo().accept(this);
    }

    public void visit(NodoOr n) {
        n.getPrimerHijo().accept(this);
        n.getUltimoHijo().accept(this);
        
        int tipo;
        int hijo_izquierdo = n.getPrimerHijo().getType();
        int hijo_derecho = n.getUltimoHijo().getType();

        if (n.getPrimerHijo() == null) {
            System.out.println("Invalido");
        }
        
        tipo = this.v_tipos.validaMultiplicacion(hijo_izquierdo, hijo_derecho);
        if (tipo == -1) {
            System.out.println("Error : or tipos erróneos");
            System.exit(0);
        }
    }

    public void visit(NodoPrint n) {
        n.getPrimerHijo().accept(this);
    }

    public void visit(NodoSalto n) {}

    public void visit(NodoMas n) {
        n.getPrimerHijo().accept(this);
        n.getUltimoHijo().accept(this);
        
        int tipo;
        int hijo_izquierdo = n.getPrimerHijo().getType();
        int hijo_derecho = n.getUltimoHijo().getType();

        if (n.getPrimerHijo() == null) {
            System.out.println("Invalido");
        }
        
        tipo = this.v_tipos.validaSuma(hijo_izquierdo, hijo_derecho);
        if (tipo == -1) {
            System.out.println("Error : Sumando tipos erróneos");
            System.exit(0);
        }
    }

    public void visit(Compuesto n) {
        for(Nodo nodo : n.getHijos().getAll()){
            nodo.accept(this);
        }
    }
}
