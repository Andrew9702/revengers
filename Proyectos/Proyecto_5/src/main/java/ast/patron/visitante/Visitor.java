package ast.patron.visitante;
import ast.patron.compuesto.*;

public interface Visitor
{
    public void visit(Hoja n);
    public void visit(HojaCadena n);
    public void visit(HojaBoolean n);
    public void visit(HojaReal n);
    public void visit(HojaEntero n);
    public void visit(HojaIdentificador n);
    
    public void visit(Nodo n);
    public void visit(NodoBinario n);
    
    public void visit(NodoIf n);
    public void visit(NodoElse n);
    public void visit(NodoWhile n);
    
    public void visit(NodoLE n);
    public void visit(NodoGR n);
    public void visit(NodoEQ n);
    public void visit(NodoGRQ n);
    public void visit(NodoLEQ n);
    public void visit(NodoMenos n);
    
    public void visit(NodoMasIgual n);
    public void visit(NodoPor n);
    public void visit(NodoDivEntera n);
    public void visit(NodoModulo n);
    public void visit(NodoDiv n);
    public void visit(NodoPotencia n);
    
    public void visit(NodoIgual n);
    public void visit(NodoDIFF n);    
    public void visit(NodoAnd n);
    public void visit(NodoNot n);
    public void visit(NodoOr n);
    
    public void visit(NodoPrint n);
    public void visit(NodoSalto n);
    
    public void visit(NodoMas n);  
    public void visit(Compuesto n);
    
    public void visit(NodoExcepcion n);
}
