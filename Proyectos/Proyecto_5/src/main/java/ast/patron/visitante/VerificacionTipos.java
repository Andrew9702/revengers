/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ast.patron.visitante;

/*
 *
 * @author andres, derian, diana
 */
public class VerificacionTipos {
    
    /*
       -1 - error
        1 - booleano
        2 - integer
        3 - real
        4 - string
    */
    
    //Reglas de uso para la suma
    int[][] suma = new int [][]{
        {-1, -1, -1, -1},
        {-1,  2,  3, -1},
        {-1,  3,  3, -1},
        {-1, -1, -1,  4}
    };
    
    //Reglas de uso para la resta
    int[][] resta = new int [][]{
        {-1, -1, -1, -1},
        {-1,  2,  3, -1},
        {-1,  3,  3, -1},
        {-1, -1, -1, -1}
    };
    
    //Reglas de uso para la multiplicacion
    int[][] multiplicacion = new int [][]{
        {-1, -1, -1, -1},
        {-1,  2,  3, -1},
        {-1,  3,  3, -1},
        {-1, -1, -1, -1}
    };
    
    //Reglas de uso para la division
    int[][] division = new int [][]{
        {-1, -1, -1, -1},
        {-1,  3,  3, -1},
        {-1,  3,  3, -1},
        {-1, -1, -1, -1}
    };
    
    //Reglas de uso para la modulo
    int[][] mod = new int[][]{
        {-1, -1, -1, -1},
        {-1,  2,  2, -1},
        {-1,  2,  2, -1},
        {-1, -1, -1, -1}
    };
    
    //Reglas de uso para la división entera
    int[][] divEntera = new int[][]{
        {-1, -1, -1, -1},
        {-1,  2,  2, -1},
        {-1,  2,  2, -1},
        {-1, -1, -1, -1}
    };
    
    //Reglas de uso para la potencia
    int[][] potencia = new int[][]{
        {-1, -1, -1, -1},
        {-1,  2,  3, -1},
        {-1,  3,  3, -1},
        {-1, -1, -1, -1}
    };
    
    //Reglas de uso para and
    int[][] and = new int[][]{
        { 1, -1, -1, -1},
        {-1, -1, -1, -1},
        {-1, -1, -1, -1},
        {-1, -1, -1, -1}
    };
    
    //Reglas de uso para or
    int[][] or = new int[][]{
        { 1, -1, -1, -1},
        {-1, -1, -1, -1},
        {-1, -1, -1, -1},
        {-1, -1, -1, -1}
    };
    
    //Regla de uso para not
    int [] not = new int[]{1, 1, 1, 1};

    //Reglas de uso para <, <=, >, >=, ==, !=
    int [][] comparadores = new int[][]{
        {-1, -1, -1, -1},
        {-1,  1,  1, -1},
        {-1,  1,  1, -1},
        {-1, -1, -1, -1}
    };
    
    //Reglas de uso para =, +=, -=
    int [][] asignacion = new int[][]{
        {-1, -1, -1, -1},
        {-1,  2,  3, -1},
        {-1,  3,  3, -1},
        {-1, -1, -1, -1}
    };
    
    /*
      Se debe acceder al indice en la tabla para saber si es valida la operacion
      entre los dos componentes
    */
    public int validaSuma(int i, int j) {
        return suma[i-1][j-1];
    }
    
    public int validaResta(int i, int j) {
        return resta[i-1][j-1];
    }

    public int validaMultiplicacion(int i, int j) {
        return multiplicacion[i-1][j-1];
    }

    public int validaDivision(int i, int j) {
        return division[i-1][j-1];
    }

    public int validaDivEntera(int i, int j) {
        return divEntera[i-1][j-1];
    }

    public int validaModulo(int i, int j) {
        return mod[i-1][j-1];
    }

    public int validaPotencia(int i, int j) {
        return potencia[i-1][j-1];
    }

    public int validaAnd(int i, int j) {
        return and[i-1][j-1];
    }

    public int validaOr(int i, int j) {
        return or[i-1][j-1];
    }

    public int validaNot(int i) {
        return not[i-1];
    }

    public int validaComparacion(int i, int j) {
        return comparadores[i-1][j-1];
    }

    public int validaAsignacion(int i, int j) {
        return asignacion[i-1][j-1];
    }   
}