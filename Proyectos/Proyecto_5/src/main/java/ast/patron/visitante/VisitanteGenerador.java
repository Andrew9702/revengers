/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ast.patron.visitante;

import ast.patron.compuesto.*;

/**
 *
 * @author andres, derian, diana
 */
public class VisitanteGenerador implements Visitor{
    
    Registros reg = new Registros();
    
    private String lineas_codigo = ".text\n";
    
    private String declaraciones = ".data\n";

    public void visit(NodoMenos n){
      Nodo hi = n.getPrimerHijo();
      Nodo hd = n.getUltimoHijo();

      String objetivo = reg.getObjetivo(n.getType());
      String[] siguientes = reg.getNsiguientes(2, n.getType());

      // Genero el código del subárbol izquiero
      reg.setObjetivo(siguientes[0], n.getType());
      hi.accept(this);

      // Genero el código del subárbol derecho
      reg.setObjetivo(siguientes[1], n.getType());
      hd.accept(this);

      String opcode =  n.getType() == 2? "sub" : "sub.s";
      
      lineas_codigo += opcode + " " + objetivo + ", " + siguientes[0] + ", " + siguientes[1]+ "\n";
    }

    public void visit(Hoja n) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    //Voy a usar los mismos registros de enteros para las cadenas
    public void visit(HojaCadena n) {
        String[] siguientes = reg.getNsiguientes(1, 2);
        lineas_codigo +="li " + siguientes[0] + ", " + n.getValor().sval+ "\n";
    }

    //Voy a usar los mismos registros de enteros para los booleanos.
    public void visit(HojaBoolean n) {
        String[] siguientes = reg.getNsiguientes(1, 2);
        lineas_codigo +="li " + siguientes[0] + ", " + n.getValor().bval+ "\n";
    }

    public void visit(HojaReal n) {
        String[] siguientes = reg.getNsiguientes(1, 3);
        lineas_codigo +="li " + siguientes[0] + ", " + n.getValor().ival+ "\n";
    }

    public void visit(HojaEntero n) {
        String[] siguientes = reg.getNsiguientes(1, 2);
        lineas_codigo +="li " + siguientes[0] + ", " + n.getValor().ival+ "\n";
    }

    public void visit(HojaIdentificador n) {}

    public void visit(Nodo n) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void visit(NodoBinario n) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void visit(NodoIf n) {
        Nodo hi = n.getPrimerHijo();
        Nodo hd = n.getUltimoHijo();

        String objetivo = reg.getObjetivo(n.getType());
        String[] siguientes = reg.getNsiguientes(2, n.getType());

        // Genero el código del subárbol izquiero
        reg.setObjetivo(siguientes[0], n.getType());
        hi.accept(this);

        // Genero el código del subárbol derecho
        reg.setObjetivo(siguientes[1], n.getType());
        hd.accept(this);
    }

    public void visit(NodoElse n) {
        Nodo hi = n.getPrimerHijo();
        Nodo hd = n.getUltimoHijo();

        String objetivo = reg.getObjetivo(n.getType());
        String[] siguientes = reg.getNsiguientes(2, n.getType());

        // Genero el código del subárbol izquiero
        reg.setObjetivo(siguientes[0], n.getType());
        hi.accept(this);

        // Genero el código del subárbol derecho
        reg.setObjetivo(siguientes[1], n.getType());
        hd.accept(this);
    }

    public void visit(NodoWhile n) {
        Nodo hi = n.getPrimerHijo();
        Nodo hd = n.getUltimoHijo();

        String objetivo = reg.getObjetivo(n.getType());
        String[] siguientes = reg.getNsiguientes(2, n.getType());

        // Genero el código del subárbol izquiero
        reg.setObjetivo(siguientes[0], n.getType());
        hi.accept(this);

        // Genero el código del subárbol derecho
        reg.setObjetivo(siguientes[1], n.getType());
        hd.accept(this);
    }

    public void visit(NodoLE n) {
        Nodo hi = n.getPrimerHijo();
        Nodo hd = n.getUltimoHijo();

        String objetivo = reg.getObjetivo(n.getType());
        String[] siguientes = reg.getNsiguientes(2, n.getType());

        // Genero el código del subárbol izquiero
        reg.setObjetivo(siguientes[0], n.getType());
        hi.accept(this);

        // Genero el código del subárbol derecho
        reg.setObjetivo(siguientes[1], n.getType());
        hd.accept(this);

        String opcode =  n.getType() == 2? "slt" : "slt.s";

        lineas_codigo += opcode + " " + objetivo + ", " + siguientes[0] + ", " + siguientes[1]+ "\n";
    }

    public void visit(NodoGR n) {
        Nodo hi = n.getPrimerHijo();
        Nodo hd = n.getUltimoHijo();

        String objetivo = reg.getObjetivo(n.getType());
        String[] siguientes = reg.getNsiguientes(2, n.getType());

        // Genero el código del subárbol izquiero
        reg.setObjetivo(siguientes[0], n.getType());
        hi.accept(this);

        // Genero el código del subárbol derecho
        reg.setObjetivo(siguientes[1], n.getType());
        hd.accept(this);

        String opcode =  n.getType() == 2? "sgt" : "sgt.s";

        lineas_codigo += opcode + " " + objetivo + ", " + siguientes[0] + ", " + siguientes[1]+ "\n";
    }

    public void visit(NodoEQ n) {
        Nodo hi = n.getPrimerHijo();
        Nodo hd = n.getUltimoHijo();

        String objetivo = reg.getObjetivo(n.getType());
        String[] siguientes = reg.getNsiguientes(2, n.getType());

        // Genero el código del subárbol izquiero
        reg.setObjetivo(siguientes[0], n.getType());
        hi.accept(this);

        // Genero el código del subárbol derecho
        reg.setObjetivo(siguientes[1], n.getType());
        hd.accept(this);

        String opcode =  n.getType() == 2? "seq" : "seq.s";

        lineas_codigo += opcode + " " + objetivo + ", " + siguientes[0] + ", " + siguientes[1]+ "\n";
    }

    public void visit(NodoGRQ n) {
        Nodo hi = n.getPrimerHijo();
        Nodo hd = n.getUltimoHijo();

        String objetivo = reg.getObjetivo(n.getType());
        String[] siguientes = reg.getNsiguientes(2, n.getType());

        // Genero el código del subárbol izquiero
        reg.setObjetivo(siguientes[0], n.getType());
        hi.accept(this);

        // Genero el código del subárbol derecho
        reg.setObjetivo(siguientes[1], n.getType());
        hd.accept(this);

        String opcode =  n.getType() == 2? "sge" : "sge.s";

        lineas_codigo += opcode + " " + objetivo + ", " + siguientes[0] + ", " + siguientes[1]+ "\n";
    }

    public void visit(NodoLEQ n) {
        Nodo hi = n.getPrimerHijo();
        Nodo hd = n.getUltimoHijo();

        String objetivo = reg.getObjetivo(n.getType());
        String[] siguientes = reg.getNsiguientes(2, n.getType());

        // Genero el código del subárbol izquiero
        reg.setObjetivo(siguientes[0], n.getType());
        hi.accept(this);

        // Genero el código del subárbol derecho
        reg.setObjetivo(siguientes[1], n.getType());
        hd.accept(this);

        String opcode =  n.getType() == 2? "sle" : "sle.s";

        lineas_codigo += opcode + " " + objetivo + ", " + siguientes[0] + ", " + siguientes[1]+ "\n";
    }

    public void visit(NodoMasIgual n) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void visit(NodoPor n) {
      Nodo hi = n.getPrimerHijo();
      Nodo hd = n.getUltimoHijo();

      String objetivo = reg.getObjetivo(n.getType());
      String[] siguientes = reg.getNsiguientes(2, n.getType());

      // Genero el código del subárbol izquiero
      reg.setObjetivo(siguientes[0], n.getType());
      hi.accept(this);

      // Genero el código del subárbol derecho
      reg.setObjetivo(siguientes[1], n.getType());
      hd.accept(this);

      String opcode =  n.getType() == 2? "mul" : "mul.s";

      lineas_codigo += opcode + " " + objetivo + ", " + siguientes[0] + ", " + siguientes[1]+ "\n";
    }

    public void visit(NodoDivEntera n) {
      Nodo hi = n.getPrimerHijo();
      Nodo hd = n.getUltimoHijo();

      String objetivo = reg.getObjetivo(n.getType());
      String[] siguientes = reg.getNsiguientes(2, n.getType());

      // Genero el código del subárbol izquiero
      reg.setObjetivo(siguientes[0], n.getType());
      hi.accept(this);

      // Genero el código del subárbol derecho
      reg.setObjetivo(siguientes[1], n.getType());
      hd.accept(this);

      String opcode =  n.getType() == 2? "div" : "div.s";

      lineas_codigo += opcode + " " + objetivo + ", " + siguientes[0] + ", " + siguientes[1]+ "\n";
    }

    public void visit(NodoModulo n) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void visit(NodoDiv n) {
      Nodo hi = n.getPrimerHijo();
      Nodo hd = n.getUltimoHijo();

      String objetivo = reg.getObjetivo(n.getType());
      String[] siguientes = reg.getNsiguientes(2, n.getType());

      // Genero el código del subárbol izquiero
      reg.setObjetivo(siguientes[0], n.getType());
      hi.accept(this);

      // Genero el código del subárbol derecho
      reg.setObjetivo(siguientes[1], n.getType());
      hd.accept(this);

      String opcode =  n.getType() == 2? "div" : "div.s";

      lineas_codigo += opcode + " " + objetivo + ", " + siguientes[0] + ", " + siguientes[1]+ "\n";
    }

    public void visit(NodoPotencia n) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void visit(NodoIgual n) {
        Nodo hi = n.getPrimerHijo();
        Nodo hd = n.getUltimoHijo();
        
        // Registro objetivo
        String objetivo = reg.getObjetivo(n.getType());
        String[] siguientes = reg.getNsiguientes(2,n.getType());

        // Genero el código del subárbol izquiero
        reg.setObjetivo(siguientes[0],n.getType());
        hi.accept(this);
        
        // Genero el código del subárbol derecho
        reg.setObjetivo(siguientes[1], n.getType());
        hd.accept(this);
        
        if(hd.getType() == 4){
            declaraciones += hi.getNombre() + ": " + " .asciiz "  + hd.getValor().sval + "\n";
        } else{
            declaraciones += hi.getNombre() + ": " + " .word "  + hd.getValor().ival + "\n";
        }
    }

    public void visit(NodoDIFF n) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void visit(NodoAnd n) {
        Nodo hi = n.getPrimerHijo();
        Nodo hd = n.getUltimoHijo();

        String objetivo = reg.getObjetivo(n.getType());
        String[] siguientes = reg.getNsiguientes(2, n.getType());

        // Genero el código del subárbol izquiero
        reg.setObjetivo(siguientes[0], n.getType());
        hi.accept(this);

        // Genero el código del subárbol derecho
        reg.setObjetivo(siguientes[1], n.getType());
        hd.accept(this);

        String opcode =  n.getType() == 2? "and" : "and.s";

        lineas_codigo += opcode + " " + objetivo + ", " + siguientes[0] + ", " + siguientes[1]+ "\n";
    }

    public void visit(NodoNot n) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void visit(NodoOr n) {
        Nodo hi = n.getPrimerHijo();
        Nodo hd = n.getUltimoHijo();

        String objetivo = reg.getObjetivo(n.getType());
        String[] siguientes = reg.getNsiguientes(2, n.getType());

        // Genero el código del subárbol izquiero
        reg.setObjetivo(siguientes[0], n.getType());
        hi.accept(this);

        // Genero el código del subárbol derecho
        reg.setObjetivo(siguientes[1], n.getType());
        hd.accept(this);

        String opcode =  n.getType() == 2? "or" : "or.s";

        lineas_codigo += opcode + " " + objetivo + ", " + siguientes[0] + ", " + siguientes[1]+ "\n";
    }

    public void visit(NodoPrint n) {
        Nodo hi = n.getPrimerHijo();
        Nodo hd = n.getUltimoHijo();
        
        // Registro objetivo
        String objetivo = reg.getObjetivo(n.getType());
        String[] siguientes = reg.getNsiguientes(2,n.getType());

        // Genero el código del subárbol izquiero
        reg.setObjetivo(siguientes[0],n.getType());
        hi.accept(this);
        
        if(n.getType() == 2){
            this.lineas_codigo += "move $a0, " + objetivo + "\nli $v0, 1\n" + "syscall\n";
        } else {
            this.lineas_codigo += "move $f12, " + objetivo + "\nli $v0, 1\n" + "syscall\n";
        }
    }

    public void visit(NodoSalto n) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void visit(NodoMas n) {
      Nodo hi = n.getPrimerHijo();
      Nodo hd = n.getUltimoHijo();

      String objetivo = reg.getObjetivo(n.getType());
      String[] siguientes = reg.getNsiguientes(2, n.getType());

      // Genero el código del subárbol izquiero
      reg.setObjetivo(siguientes[0], n.getType());
      hi.accept(this);

      // Genero el código del subárbol derecho
      reg.setObjetivo(siguientes[1], n.getType());
      hd.accept(this);

      String opcode =  n.getType() == 2? "add" : "add.s";

      lineas_codigo += opcode + " " + objetivo + ", " + siguientes[0] + ", " + siguientes[1]+ "\n";
    }
    
    public void visit(Compuesto n) {
        for(Nodo nodo : n.getHijos().getAll()){
            nodo.accept(this);
        }
    }

    public void visit(NodoExcepcion n) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public String getLineas(){
        return this.lineas_codigo;
    }
    
    public String getDeclaraciones(){
        return this.declaraciones;
    }
    
}
