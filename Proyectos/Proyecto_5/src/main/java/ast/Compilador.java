//import Parser;
package ast;
import java.io.*;
import ast.patron.compuesto.*;
import ast.patron.visitante.*;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Compilador{

    Parser parser;
    Nodo raízAST;
    VisitorPrint v_print;
    VisitorTypes v_types;
    VisitanteGenerador v_generador;
    
    String nombreArchivo;

    Compilador(Reader fuente){
        parser = new Parser(fuente);
        v_print = new VisitorPrint();
        v_types = new VisitorTypes();
        v_generador = new VisitanteGenerador();
    }

    public void ConstruyeAST(boolean debug){
        parser.yydebug = debug;
        parser.yyparse(); // análisis léxico, sintáctio y constucción del AST
        raízAST = parser.raíz;
    }

    public void imprimeAST(){
        parser.raíz.accept(v_types);
        parser.raíz.accept(v_generador);
        
        String archivo = "src/main/resources/resultado";
        
        String lineas_codigo = v_generador.getDeclaraciones() + "\n" + v_generador.getLineas();
        
        System.out.println("-----Codigo generado para ensamblador:------\n" + lineas_codigo);
        FileWriter fw;
        try {
            fw = new FileWriter(archivo.substring(0, archivo.length()-2) + ".asm");
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(lineas_codigo);
            bw.flush();
            bw.close();
        } catch (IOException ex) {
            Logger.getLogger(Compilador.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void main(String[] args){
            String archivo = "src/main/resources/test.p";
        try{
            Reader a = new FileReader(archivo);
            Compilador c  = new Compilador(a);
            c.ConstruyeAST(true);
            c.imprimeAST();
        }catch(FileNotFoundException e){
            System.err.println("El archivo " + archivo +" no fue encontrado. ");
        }catch(ArrayIndexOutOfBoundsException e){
            System.err.println("Uso: java Compilador [archivo.p]: ");
        }
    }
}
