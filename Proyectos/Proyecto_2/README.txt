Para probar el programa se debera hacer lo siguiente desde la consola:

	Desde la carpeta donde este la practica 2 se debera escribir lo siguiente:
		
		mvn clean initialize compile

	Una vez que se haya compilado correctamente se escribe:

		mvn exec:java -Dexec.mainClass=asintactico.Parser -Dexec.args="fizzbuzz.p"

	Donde fizzbuzz.p es nuestro archivo de pruebas por lo que para hacer cualquier prueba se debe
	modificar este archivo.
