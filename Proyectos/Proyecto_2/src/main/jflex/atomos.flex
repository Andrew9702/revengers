package asintactico;
import java.util.Stack;
%%

%public
%class Flexer
%unicode
%standalone
%debug
%byaccj
%line
%state INDENTA DEINDENTA CADENA CUERPO

%{

//Creamos una pila donde se almacenaran los espacios encontrados.
static Stack<Integer> pila = new Stack<Integer>();

//Inicializamos en cero el numero de espacios.
static int espacios=0;
static int linea = 1;
static int n_indents = 0;
static int n_deindents = 0;

private Parser yyparser;

    public Flexer(java.io.Reader r, Parser parser){
    	   this(r);
    	   yyparser = parser;
    }

/**
    Metodo que almacena en una pila el numero
    de espacios en blanco antes del inicio de una linea.
*/
public void espacios(int esp){
    //Inicializamos la pila con 0 espacios en blanco
    // y el numero de espacios en parametro.
    if(pila.empty()){ 
             pila.push(0);
    } else {
        //Revisamos si el parametro es mayor al ultimo
        // numero de espacios introducido en la pila, esto 
        // significa el inicio de un bloque
        if(esp > pila.peek()){
            pila.push(esp);
            yybegin(CUERPO);
            n_indents = 1;
        //Revisamos si es el mismo numero
        // lo cual indicaria que la linea pertenece al mismo bloque.
        } else if(esp == pila.peek()) {
            yybegin(CUERPO);
        } else {
        //En el caso de ser menor entonces sacamos el espacio
        //de la pila y significa una deindentacion.
        //Pero si llegamos al primer elemento de la pila y los espacios
        //aun son mas que cero, significa un error de indentacion, en caso contrario
        //significa el fin de un bloque
            while(esp < pila.peek() && pila.peek() != 0){
                pila.pop();
                n_deindents++;
            }
            if(pila.peek() == esp){
                yybegin(DEINDENTA);
            } else {
                System.out.print("\n ***Error: Mala indentación*** " + " Linea: " + linea + "\n");
                System.exit(0);
            }
        }
    }
}
%}

BOOLEANO      = ("True" | "False")
ENTERO        = [1-9][0-9]* | 0+
REAL          = {ENTERO}? \. {ENTERO}
OPERADOR  		=       ("+" | "-" | "*" | "**" | "/" | "//" | "%" |
			         "<" | ">" | "<=" | "+=" | "-=" | ">=" | "==" | "!=" | "=" )
MAS           = "+"
MULT          = "*"
MENOS         = "-"
DIV           = "/"
IGUAL         = "="
MAYOR         = ">"
MENOR         = "<"
MAYORQ        = ">="
MENORQ        = "<="
IGUALIGUAL    = "=="
MODULO        = "%"
DIFERENTE     = "!="
POT           = "**"
FLOOR         = "//"
AND           = and
OR            = or
NOT           = not
WHILE         = while
IF            = if
ELSE          = else
PRINT         = print
CARACTER      = ([:letter:] | [:digit:] | "_" | "$" | " " | "#" | {OPERADOR} | {SEPARADOR})
IDENTIFICADOR = ([:letter:] | _ ) ([:jletter:] | [:digit:] | _ )*
SEPARADOR     = \:
SALTO         = "\n"
ESPACIO       = " "
PIz           = "(" 
PDr           =  ")"

%%

//Caso de un comentario
#.*                                      {}

//Caso de una cadena, checamos que termine con unas comillas y tenga caracteres entre de esas comillas
//Si se encuentra un salto de linea entre las comillas entonces mandamos error
<CADENA>{
  {CARACTER}*\"                         { yybegin(CUERPO); return Parser.CADENA;}
  {SALTO}				{ System.out.println("\n*** ERROR DE CADENA ***" + " Linea: " + linea ); System.exit(1);}
}

//Checamos el caracter inicial, si es un espacio entonces mandamos un error
//de lo contrario empieza con el cuerpo del codigo
<YYINITIAL>{
  " "+                        		{ System.out.println("\n***Error: Mala indentación*** " + " Linea: " + linea ); System.exit(1);}
  .                               	{ yypushback(1); yybegin(CUERPO);}
}

//Checamos los matches que se encuentren en el texto con los tokens definidos arriba
//y regresamos los tokens del parser.
<CUERPO>{
   \"					{ yybegin(CADENA); }
  {ENTERO}				{ return Parser.ENTERO; }
  {REAL}     				{ return Parser.REAL; }
  {MAS}                                  { return Parser.MAS; }
  {MENOS}                                { return Parser.MENOS; } 
  {MULT}                                 { return Parser.MULT; }
  {POT}                                  { return Parser.POT; }
  {DIV}                                  { return Parser.DIV; }
  {FLOOR}                                { return Parser.FLOOR; }
  {MODULO}                               { return Parser.MODULO; }
  {MENOR}                                { return Parser.MENOR; }
  {MAYOR}                                { return Parser.MAYOR; }
  {MAYORQ}                               { return Parser.MAYORQ; }
  {MENORQ}                               { return Parser.MENORQ; }
  {IGUAL}                                { return Parser.IGUAL; }
  {DIFERENTE}                            { return Parser.DIFERENTE; }
  {IGUALIGUAL}                           { return Parser.IGUALIGUAL; }
  {SEPARADOR}                            { return Parser.SEPARADOR; }
  {PIz}                                  { return Parser.PIz;}
  {PDr}                                  { return Parser.PDr;}
  {AND}                                  { return Parser.AND; }
  {OR}                                   { return Parser.OR; }
  {NOT}                                  { return Parser.NOT; }
  {WHILE}                                { return Parser.WHILE; }
  {IF}                                   { return Parser.IF; }
  {ELSE}                                 { return Parser.ELSE; }
  {PRINT}                                { return Parser.PRINT; }
  {BOOLEANO}                 		{ return Parser.BOOLEANO; }
  {IDENTIFICADOR}           		{ return Parser.IDENTIFICADOR; }
  {SALTO}                 		{ yybegin(INDENTA); espacios=0; return Parser.SALTO; }
  {ESPACIO}                              {}
}

//Caso para la indentacion
//Si vemos un salto entonces reseteamos los espacios a 0 y empezamos a contar
//los espacios hasta que aparezca algo distinto a un espacio en blanco. En cuyo caso
//contamos los indenta y deindenta con la funcion espacios y pedimos al parser el token INDENTA
<INDENTA>{
  {SALTO}                           { espacios = 0; }
  " "                               { espacios++; }
  \t                                { espacios += 4; }
  .                                 {yypushback(1);
                                     espacios(espacios);
                                     if(n_indents == 1){
                                      n_indents = 0;
                                      return Parser.INDENTA;
                                     }
                                    }
}

//Caso para la deindentacion
//revisamos si el resultado de los deindenta de espacios es mayor a 0
//si si entonces restamos y regresamos el token deindenta.
<DEINDENTA>{
  .                                 {yypushback(1);
                                     if(n_deindents > 0){
                                      n_deindents--;
                                      return Parser.DEINDENTA;
                                     }
                                    }
}

//Caso al final del archivo
//los espacios del final son 0, revisamos que el numero de deindents
//sea menor a 0 de no serlo restamos y regresamos el token deindenta.
//si si salimos.
<<EOF>>                             { espacios(0);
					    if(n_deindents > 0){
					      n_deindents--;
					      return Parser.DEINDENTA;
					    }else{
                                              return 0;
				            }
                                    }

//Caso en el que no haga match con ninguna regla.
.                                   { System.out.println("***ERROR LÉXICO*** " + " linea:" + linea) ; System.exit(1);}

