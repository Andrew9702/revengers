%{
import java.lang.Math;
import java.io.*;

%}

%token IDENTIFICADOR ENTERO CADENA REAL BOOLEANO PIz PDr
%token SALTO PRINT IF SEPARADOR ELSE WHILE INDENTA DEINDENTA
%token OR AND NOT 
%token MENOR MAYOR IGUAL MAYORQ MENORQ DIFERENTE MAS MENOS MULT DIV MODULO FLOOR POT IGUALIGUAL

%%

/*Se aniade la gramatica del pdf*/
file_input : SALTO 
           | stmt
           | file_input SALTO
           | file_input stmt
;

stmt : simple_stmt 
     | compound_stmt   
;

simple_stmt : small_stmt SALTO
;

small_stmt: expr_stmt
          | print_stmt
;

expr_stmt : test
          | test IGUAL test
;

print_stmt : PRINT test
;

compound_stmt : if_stmt
              | while_stmt
;

if_stmt : IF test SEPARADOR suite
        | IF test SEPARADOR suite ELSE SEPARADOR suite
;

while_stmt : WHILE test SEPARADOR suite
;

suite : simple_stmt
      | SALTO INDENTA stmt_1 DEINDENTA
;

stmt_1 : stmt
       | stmt stmt_1
;

test : or_test
;

or_test : and_test
        | and_test or_test_1
;

or_test_1 : OR or_test
;

and_test : not_test
          | not_test and_test_1
;

and_test_1 : AND and_test
;

not_test : NOT not_test
         | comparison
;

comparison : expr 
           | expr comparison_1
;

comparison_1 : comp_op comparison
;

comp_op : MENOR
        | MAYOR
        | MAYORQ
        | MENORQ 
        | DIFERENTE
        | IGUALIGUAL
;

expr : term 
     | term expr_1
;

expr_1 : MAS expr
       | MENOS expr
;

term : factor
     | factor term_1
;

term_1 : MULT term
       | DIV term
       | MODULO term
       | FLOOR term
;

factor : MAS factor
       | MENOS factor
       | power
;

power : atom
      | atom POT factor
;

atom : IDENTIFICADOR
     | ENTERO
     | CADENA
     | REAL
     | BOOLEANO
     | PIz test PDr
;

%%

private Flexer alexico;

// Regresar átomos
private int yylex() {
  int yyl_return = -1;

  try{
    yyl_return = alexico.yylex();

  }catch (IOException e){
    System.err.println("Error de IO." + e);
  }
  return yyl_return;
}

public void yyerror (String error){
  System.err.println("[ERROR] " +error);
  System.exit(2);
}

public Parser(Reader r){
  alexico = new Flexer(r,this);
}

public static void main(String args[]){
  try{
   Parser yyparser = new Parser(new FileReader("src/main/resources/fizzbuzz.p"));
   yyparser.yydebug = true; //Depuracion
   yyparser.yyparse();
  }catch(FileNotFoundException e){
    System.err.println("El Archivo " + " no existe.");
  }

}